<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form</title>
  </head>
  <body>
    <h1>Buat Account Baru</h1>

    <form action="/welcome" method="post">
      @csrf
      <label for="first">First name:</label><br /><br />
      <input type="text" id="first" name="firstname" /> <br /><br />

      <label for="last">Last name:</label><br /><br />
      <input type="text" id="last" name="lastname" /><br /><br />

      <label for="gender">Gender</label><br /><br />
      <input type="radio" />Male <br />
      <input type="radio" />Female <br />
      <input type="radio" />Other <br /><br />

      <label for="nationality">Nationality</label><br /><br />
      <select name="nationality" id="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="english">English</option></select
      ><br /><br />

      <label for="language">Language Spoken:</label><br /><br />
      <input type="checkbox" /> Bahasa Indonesia <br />
      <input type="checkbox" /> English <br />
      <input type="checkbox" /> Other<br /><br />

      <label for="bio">Bio:</label><br /><br />
      <textarea id="bio" cols="30" rows="10"></textarea><br />
      
      
      <input type="submit" value="Sign Up">
    </form>
  </body>
</html>
